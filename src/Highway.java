import java.util.Random;
import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
 
public class Highway extends Applet{
	Image backGroundimage;
	int highwayLength;
	int totalCar;		//waiting behind starting line
	Car[] cars;
	Highway_Controller hc;
	public void init(){
		highwayLength = 1300;
		totalCar = 30;
		hc = new Highway_Controller(highwayLength, totalCar);
		cars = new Car[totalCar];
		int i;
		for(i=0;i<totalCar;i++)
			cars[i] = new Car((i+1),hc);
		for(i=0;i<totalCar;i++)
			cars[i].start();
		backGroundimage = getImage(getCodeBase(),"graph/Highway.png");
	}
    public void paint(Graphics g) {
        //g.drawString("Hello, this is a program of Java Applet!", 40, 10);
		int i;
		int[] pos;
		Image[] carImage;
		for(i=0;i<100000;i++){
			//System.out.println("Arrrrrrrrrrrrrrrrrrrrrr");
			try{
				Thread.sleep(100);
			}catch(InterruptedException e){
				;
			}
			super.paint(g);
			g.drawImage(backGroundimage,0,0,this);
			int LC = hc.getLastCar();
			pos = hc.getAllPosition();
			carImage = hc.getAllImage();
			//System.out.println("Last Car on Road: "+LC);
			int j;
			for(j=0;j<LC;j++)
				g.drawImage(carImage[j],pos[j],0,this);
		}
    }
} 	