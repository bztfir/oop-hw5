import java.util.Random;
import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
 
public class Highway_Controller{
	private int highwayLength;
	private int totalCar;
	private int last_car_on_road;
	private int[] map;
	private int[] position;
	Image[] carImage;
	public Highway_Controller(int highwayLength, int totalCar){
		this.highwayLength = highwayLength;
		this.totalCar = totalCar;
		carImage = new Image[totalCar];
		position = new int[totalCar];
		int i;
		for(i=0;i<totalCar;i++)
			carImage[i] = null;
		last_car_on_road = 0;
		map = new int[highwayLength];
		this.setMap(0,highwayLength,-1);
	}
	public void setMap(int start, int end, int value){
		int i=start;
		int j=end;
		if(i < 0)
			i = 0;
		if(j >= highwayLength)
			j = highwayLength-1;
		for(;i<j;i++)
			map[i] = value;
	}
	public int[] getMap(){
		return map;
	}
	public int getHighwayLength(){
		return highwayLength;
	}
	public int getLastCar(){
		return last_car_on_road;
	}
	public void setLastCar(int last_car_on_road){
		this.last_car_on_road = last_car_on_road;
	}
	public void addImage(int i, Image x){
		carImage[i] = x;
	}
	public Image[] getAllImage(){
		return carImage;
	}
	public void setPosition(int i, int value){
		position[i] = value;
		//System.out.println("position["+i+"] = "+value);
	}
	public int[] getAllPosition(){
		return position;
	}
}