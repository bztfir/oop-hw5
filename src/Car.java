import java.util.Random;
import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Car extends Thread{
	private final int MAX_SPEED = 30;
	private int speed;
	private int position;
	private int length;
	private Image Car_image;
	private String path;	//path of graph
	private int carNumber;
	int i;
	Random rand_num;
	Highway_Controller hc;
	public Car(int carNumber, Highway_Controller hc){
		this.hc = hc;
		this.carNumber = carNumber;
		speed = 0;
		rand_num = new Random();
		int x = (rand_num.nextInt(10)+1);
		path = "graph/c"+x+".png";
		ClassLoader classLoader = this.getClass().getClassLoader(); 
		java.net.URL imageURL = classLoader.getResource(path);
		Car_image = (new ImageIcon(imageURL)).getImage();
		length = (new ImageIcon(Car_image)).getIconWidth();
		position = (-1*(length));
	}
	public void run(){
		int oldPosition = 0;
		int oldspeed = 0;
		while(true){
			synchronized(hc){
				oldspeed = speed;
				int[] map = hc.getMap();
				if(carNumber <= hc.getLastCar()){
					//calculate new speed and position
					oldPosition = position;
					position += speed;
					if(position >= hc.getHighwayLength()){
						hc.setMap(oldPosition,(oldPosition+length),-1);
						hc.setPosition((carNumber-1), -500);
						break;
					}
					for(i=(position+length);i<hc.getHighwayLength();i++)
						if(map[i] == (carNumber-1))
							break;
					if(i >= (hc.getHighwayLength()-1))
						speed = MAX_SPEED;
					else{
						i -= (position+length);
						speed = (int)(i/4);
						if(speed > MAX_SPEED)
							speed = MAX_SPEED;
					}
					if(rand_num.nextInt(30) == 0)	//suddenly stops the car
						speed = 0;
					hc.setMap(oldPosition,(oldPosition+length),-1);
					hc.setMap(position,(position+length),carNumber);
					hc.setPosition((carNumber-1), position);
				}
				else if(carNumber == (hc.getLastCar()+1)){
					//check front car's position
					for(i=0;i<hc.getHighwayLength();i++)
						if(map[i] == (carNumber-1))
							break;
					if(i >= 2){	//add car
						System.out.println("add car: "+carNumber);
						speed = (int)(i/2);
						if(speed > MAX_SPEED)
							speed = MAX_SPEED;
						position = (speed-length);
						hc.addImage((carNumber-1),Car_image);
						hc.setPosition((carNumber-1), position);
						hc.setMap(0,speed,carNumber);
						hc.setLastCar(carNumber);
					}
				}
				if(speed > (oldspeed+5))
					speed = (oldspeed+5);
			}
			sleep(100);
		}
	}
	private void sleep(int time){
		try{
			Thread.sleep(time);
		}catch(InterruptedException e){
			;
		}
	}
}