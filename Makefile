all:
	javac src/*.java
	mv src/*.class ./
	jar -cf code.jar Highway.class Car.class Highway_Controller.class ./graph
	rm -f *.class
run:
	appletviewer test.html
clean:
	rm -f code.jar